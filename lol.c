/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lol.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mploux <mploux@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/26 21:25:52 by mploux            #+#    #+#             */
/*   Updated: 2016/11/26 21:29:05 by mploux           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"
#include <stdlib.h>
#include <unistd.h>

static int	read_line(const int fd, char **before, char **after)
{
	char	*buff;
	int		ret;
	int		cptr;
	char	*tmp;
	int		size;

	if (!(buff = (char *)ft_memalloc(sizeof(char) * (BUFF_SIZE + 1))))
		return (-1);
	cptr = 0;
	while ((ret = read(fd, buff + cptr, BUFF_SIZE)) > 0)
	{
		cptr += ret;
		buff = (char *)ft_realloc(buff, cptr + BUFF_SIZE);
		buff[cptr] = 0;
		if ((tmp = ft_strchr(buff, '\n')))
		{
			size = tmp - buff;
			*after = ft_strdup(tmp + 1);
			*before = ft_strsub(buff, 0, size);
			ft_strdel(&buff);
			return (size);
		}
	}
	ft_strdel(&buff);
	return (ret);
}

static int	find_line_in_rest(char **rest, char **line)
{
	char *tmp;
	char *new_rest;

	if (!(*rest))
		return (0);
	if ((tmp = ft_strchr(*rest, '\n')))
	{
		*line = ft_strsub(*rest, 0, tmp - (*rest));
		new_rest = ft_strdup(tmp + 1);
		ft_strdel(rest);
		*rest = new_rest;
		return (1);
	}
	else
		return (0);
	return (-1);
}

int			get_next_line(const int fd, char **line)
{
	static char *rest = NULL;
	int			ret;
	char		*before;
	char		*after;

	if (!(ret = find_line_in_rest(&rest, line)))
	{
		ret = read_line(fd, &before, &after);
		if (rest)
			*line = ft_strjoin(rest, before);
		else
			*line = ft_strdup(before);
		ft_strdel(&rest);
		if (!rest)
			rest = after;
		return (1);
	}
	return (ret);
}
