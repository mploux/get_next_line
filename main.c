/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mploux <mploux@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/25 14:46:16 by mploux            #+#    #+#             */
/*   Updated: 2016/11/28 05:52:54 by mploux           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>

char	*read_file(const int fd)
{
	char	*buff;
	char	nbuff[BUFF_SIZE];
	int		ret;
	int		cptr;

	// if (!(buff = ft_strnew(BUFF_SIZE - 2)))
	// 	return (0);
	cptr = 0;
	buff = NULL;
	while ((ret = read(fd, nbuff, BUFF_SIZE)) > 0)
	{
		cptr += ret;
		buff = ft_realloc(buff, cptr + BUFF_SIZE);
		ft_strncat(buff, nbuff, ret);
		buff[cptr] = 0;

		// if (ft_strchr(buff + cptr - ret, '\n') != 0)
		// 	break;
		// printf("YOLLLOOOOOOO: %s         %i\n\n", buff, cptr);
	}
	buff[cptr] = 0;
	return (buff);
}

int     main(const int ac, char **av)
{
	int fd[ac - 1];
	char *line = NULL;
	int i;

	if (ac < 2)
	{
		ft_putstr("2 ARGUMENTS ASS HOLE !");
		return (0);
	}

	i = -1;

	while (++i < ac - 1)
		fd[i] = open(av[i + 1], O_RDONLY);

	// char *buff = ft_strnew(100);
	// while (read(fd, buff, 100) > 0)
	// {
	// 	ft_putstr(buff);
	// }

	// get_next_line(fd, &line);
	// ft_putstr(line);
	// ft_putstr("\n");
	// get_next_line(fd2, &line);
	// ft_putstr(line);
	// ft_putstr("\n");
	//
	// get_next_line(fd, &line);
	// ft_putstr(line);
	// ft_putstr("\n");
	// get_next_line(fd2, &line);
	// ft_putstr(line);
	// ft_putstr("\n");
	//
	// get_next_line(fd, &line);
	// ft_putstr(line);
	// ft_putstr("\n");
	// get_next_line(fd2, &line);
	// ft_putstr(line);
	// ft_putstr("\n");

	// ft_putnbr(fd);
	// ft_putstr("\n");
	i = 0;
	char test = ac;
	while (test)
	{
		if (get_next_line(fd[i % (ac - 1)], &line) > 0)
		{
			write(1, line, ft_strlen(line));
			ft_putstr("$---END---$\n");
			ft_strdel(&line);
		}
		else
			test--;
		i++;
	}

	// line = read_file(fd);
	// ft_putstr(line);
	// ft_putstr("\n");
	// printf("%s\n", line);

	return (0);
}
