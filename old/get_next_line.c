/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mploux <mploux@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/09 13:25:20 by mploux            #+#    #+#             */
/*   Updated: 2016/11/11 18:59:28 by mploux           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#define BUFF_SIZE 4
#include "libft/libft.h"
#include <libc.h>
#include "get_next_line.h"
//
// char	*ft_dupafter1(const char *s, char c, int size)
// {
// 	size_t i;
//
// 	i = 0;
// 	while (s[i] && i < size)
// 	{
// 		if (s[i] == c)
// 			return (ft_strdup((char *)&s[i + 1]));
// 		if (s[i + 1] == c)
// 			return (ft_strdup((char *)&s[i + 2]));
// 		i++;
// 	}
// 	return (0);
// }
// int		get_next_line1(const int fd, char **line)
// {
// 	char		buff[BUFF_SIZE + 1];
// 	static char	*save;
// 	char		*tmp;
// 	int			str_size;
// 	int			save_size;
// 	int			buff_size;
//
// 	tmp = NULL;
// 	save_size = 0;
// 	buff_size = 0;
// 	str_size = 0;
// 	while (read(fd, buff, BUFF_SIZE) > 0)
// 	{
// 		if (save)
// 			while (save[save_size] != '\n' && save[save_size])
// 				save_size++;
// 		buff_size = 0;
// 		while (buff_size < BUFF_SIZE && buff[buff_size - 1] != '\n')
// 			buff_size++;
// 		str_size += buff_size;
// 		*line = ft_strnew(str_size + save_size);
// 		if (save)
// 			ft_strncat(*line, save, save_size);
// 		if (tmp)
// 			ft_strcpy(*line, tmp);
// 		ft_strncat(*line, buff, buff_size);
// 		free(tmp);
// 		free(save);
// 		if ((save = ft_dupafter(buff, '\n', BUFF_SIZE)) != 0)
// 			return (1);
// 		tmp = ft_strdup(*line);
// 	}
// 	free(tmp);
// 	return (0);
// }

char		*ft_join(char const *s1, char const *s2)
{
	int		i;
	int		l1;
	int		l2;
	char	*data;

	i = 0;
	if (!s1 && s2)
		return (ft_strdup(s2));
	if (!s1 || !s2)
		return (NULL);
	l1 = ft_strlen(s1);
	l2 = ft_strlen(s2);
	if (!(data = ft_strnew(l1 + l2 + 1)))
		return (NULL);
	while (i < l1 + l2)
	{
		if (i < l1)
			data[i] = s1[i];
		else
			data[i] = s2[i - l1];
		i++;
	}
	data[i] = '\0';
	return (data);
}

char		*ft_strbefore(char *str, char c)
{
	int i;
	int len;
	char *new;

	i = -1;
	len = 0;
	while (str[len] != c && str[len])
		len++;
	new = ft_strnew(len);
	while (++i < len)
		new[i] = str[i];
	return (new);
}

char		*ft_strafter(char *str, char c)
{
	size_t i;

	i = 0;
	while (str[i])
	{
		if (str[i] == c)
			return (ft_strdup((char *)&str[i + 1]));
		if (str[i + 1] == c)
			return (ft_strdup((char *)&str[i + 2]));
		i++;
	}
	return (NULL);
}

int			get_next_line(const int fd, char **line)
{
	char		buff[BUFF_SIZE + 1] = {0};
	char		*new_line;
	static char	*save;
	char		*before;

	new_line = NULL;
	// ft_putstr(save);
	// ft_putstr("\n");
	while (read(fd, buff, BUFF_SIZE))
	{
		if (save)
		{
			new_line = ft_join(new_line, save);
			free(save);
		}
		new_line = ft_join(new_line, buff);
		ft_strclr(buff);
		if (ft_strchr(new_line, '\n') != 0)
		{
			before = ft_strbefore(new_line, '\n');
			save = ft_strafter(new_line, '\n');
			// ft_putstr("\nbefore: |");
			// ft_putstr(before);
			// ft_putstr("|\nafter: |");
			// ft_putstr(save);
			// ft_putstr("|\nline: |");
			*line = ft_join(*line, before);
			// ft_putstr(*line);
			// ft_putstr("|\n");
			free(new_line);
			free(before);
			return (1);
		}
	}
	return (0);
}

#include <fcntl.h>
int		main(int ac, char **av)
{
	int fd;
	char *line;
	fd = open(av[1], O_RDONLY);

	// get_next_line(fd, &line);
	// ft_putstr(line);
	// ft_putstr("\n");
	// free(line);
	// line = NULL;
	// get_next_line(fd, &line);
	// ft_putstr(line);
	// ft_putstr("\n");
	// free(line);
	// line = NULL;
	// get_next_line(fd, &line);
	// get_next_line(fd, &line);
	// get_next_line(fd, &line);
	// get_next_line(fd, &line);
	// get_next_line(fd, &line);
	// get_next_line(fd, &line);
	// get_next_line(fd, &line);
	// get_next_line(fd, &line);
	// get_next_line(fd, &line);
	// get_next_line(fd, &line);
	while (get_next_line(fd, &line))
	{
		ft_putstr(line);
		ft_putstr("\n");
		free(line);
		line = NULL;
	}
	return (0);
}
