/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mploux <mploux@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/26 21:18:54 by mploux            #+#    #+#             */
/*   Updated: 2016/11/28 02:53:40 by mploux           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef GET_NEXT_LINE
# define GET_NEXT_LINE
# define BUFF_SIZE 10
# include "libft/libft.h"

typedef struct			s_gnl
{
	int				fd;
	char			*rest;
	struct s_gnl	*next;
}						t_gnl;

int			get_next_line(const int fd, char **line);

#endif
